import java.util.*;

/**
 * Idée en vrac et points d'amélioration + stratégie
 * <p>
 * Prendre en compte le deuxième joueur (qui peut nous piquer l'ingrédient dans le four, ou l'assiette posée sur une table
 * Prendre en compte les objets posés sur les tables (pour augmenter la rapidité d'execution des commandes et tente de collaborer avec le deuxième joueur
 * <p>
 * Trouver un moyen de stopper l'execution d'une recette complètement
 * Vérifier que la commande est toujours présente avant d'envoyer au client pour ne pas perdre de point inutilement et garder la commande de côté
 * <p>
 * Diminuer les temps de trajets et les temps d'attentes
 * Optimiser les déplacement
 * Detecter lorsque le chemin est bloqué pour un des 2 joueurs et tenter de trouver la meilleur solution (attendre, faire le tour ou se pousser)
 * <p>
 * Voilà gros simon c'est tout ce que j'ai comme idée pour l'instant
 * Si tu vois autre chose, hésites pas ;)
 */
class Player {
    //J'ai foutu tous ces objets en statique pour que ce soit utilisable dans mes méthodes plus bas pour gérer les recettes, les assiettes etc...
    private static final Map<String, List<int[]>> recipes = new HashMap<>();
    private static int playerX = 0;
    private static int playerY = 0;
    private static final int[] blueBerry = new int[2];
    private static final int[] iceCream = new int[2];
    private static final int[] strawberry = new int[2];
    private static final int[] dishes = new int[2];
    private static final int[] knife = new int[2];
    private static final int[] client = new int[2];
    private static final int[] oven = new int[2];
    private static final int[] dough = new int[2];
    private static int turnsRemaining = 1000;
    private static String playerItem = "";
    private static int partnerX = 0;
    private static int partnerY = 0;
    private static String partnerItem = "";
    private static int numTablesWithItems = 0; // the number of tables in the kitchen that currently hold an item
    private static String ovenContents = "";
    private static int ovenTimer = 0;
    private static int numCustomers = 0; // the number of customers currently waiting for food
    private static String secondCommand = "";
    private static final List<String> freeTables = new ArrayList<>();
    private static List<String> occupiedTables = new ArrayList<>();
    private static Map<String, String> occupiedTables2 = new HashMap<>();
    private static final Scanner in = new Scanner(System.in);
    private static final List<String> floor = new ArrayList<>();
    private static Map<String, Integer> reachedTiles = new HashMap<>();
    
    //La liste des recettes réalisable sans transformation
    private static final List<String> simpleItems = Arrays.asList("BLUEBERRIES", "ICE_CREAM");

    //Debug booleen
    private static final boolean debug = false;
    //Init des variables utilisé plus tars
    private static String currentCommand = "";
    private static List<String> currentCommandSteps = new ArrayList<>();
    private static int currentStep = 1;
    private static boolean droppedDish = false;
    private static int[] droppedDishPosition = new int[2];

    public static void main(String[] args) {
        //Initialisation des commandes
        int numAllCustomers = in.nextInt();
        for (int i = 0; i < numAllCustomers; i++) {
            String customerItem = in.next();
            int customerAward = in.nextInt();
        }
        in.nextLine();
        Map<String, int[]> arrayMaps = new HashMap<>();
        arrayMaps.put("B", blueBerry);
        arrayMaps.put("I", iceCream);
        arrayMaps.put("D", dishes);
        arrayMaps.put("W", client);
        arrayMaps.put("S", strawberry);
        arrayMaps.put("C", knife);
        arrayMaps.put("H", dough);
        arrayMaps.put("O", oven);
        //Initialisation des ingredients et ustensiles etc...
        for (int i = 0; i < 7; i++) {
            String kitchenLine = in.nextLine();
            String[] s = kitchenLine.split("");
            int l = s.length;
            for (int j = 0; j < l; j++) {
                int[] item = arrayMaps.get(s[j]);
                if (item != null) {
                    item[1] = i;
                    item[0] = j;
                } else if (s[j].equals("#")) {
                    freeTables.add(j + "" + i);
                } else {
                    floor.add(j + "" + i);
                }
            }
            System.err.println(kitchenLine);
        }
        System.err.println("floor :"+floor);

        //Initialisation du cahier de recettes (chaque produit fini possède une liste d'action pour sa réalisation)
        recipes.put("BLUEBERRIES", Collections.singletonList(blueBerry));
        recipes.put("ICE_CREAM", Collections.singletonList(iceCream));
        recipes.put("CHOPPED_STRAWBERRIES", Arrays.asList(strawberry, knife));
        recipes.put("CROISSANT", Arrays.asList(dough, oven));
        recipes.put("TART", Arrays.asList(dough, knife, blueBerry, oven));

        
        while (true) {
            if ("".equals(currentCommand)) initInput();
            
            
            
            
            
            
            
            
            
            
            int[] position = {playerX, playerY};
            boolean partner = true;
            reachedTiles = new HashMap<>();
            getReachableTiles(Collections.singletonList(position), partner, 1);
            int dist = getDistanceTo(position, iceCream, partner);
            System.err.println("Distance to IceCream :"+dist);
            dist = getDistanceTo(position, oven, partner);
            System.err.println("Distance to oven :"+dist);
            dist = getDistanceTo(position, dishes, partner);
            System.err.println("Distance to dishes :"+dist);
            dist = getDistanceTo(position, knife, partner);
            System.err.println("Distance to knife :"+dist);
            dist = getDistanceTo(position, client, partner);
            System.err.println("Distance to client :"+dist);
            
            
            
            
            
            
            
            

            //Test qui permet de valider le fonctionnement des méthodes
            //Pour ce test, on essais de passer une commande du début à la fin
            //Amélioration : essayer plusieurs recettes différentes
            if (debug) {
                makeRecipe("CHOPPED_STRAWBERRIES");
                getNewDish();
                makeRecipe("ICE_CREAM");
                droppedDishPosition = dropDish();
                makeRecipe("CROISSANT");
                putItemOnDroppedDish(droppedDishPosition);
                makeRecipe("TART");
                putItemOnDroppedDish(droppedDishPosition);
                getDroppedDish(droppedDishPosition);
                sendToCustomer();
            }

            //Si on a pas encore de commande en cours, alors on enregistre la commande que l'on va traiter (toujours la deuxième)
            //Amélioration : choisir une commande de façon plus maline (plus de point ? Plus rapide a confectionner ?)
            if ("NONE".equals(playerItem) && "".equals(currentCommand)) {
                currentCommand = secondCommand;
                //Si la commande contient des fraises, on va les placer en première position. (ça me semblait malin, mais plus trop maintenant)
                //Amélioration : Ordonnancer les ingrédients de la commande pour réduire les trajets et diminuer les temps d'attentes
                if (currentCommand.contains("CHOPPED_STRAWBERRIES")) {
                    currentCommand = currentCommand.replace("-CHOPPED_STRAWBERRIES", "");
                    currentCommand = currentCommand.replace("DISH", "DISH-CHOPPED_STRAWBERRIES");
                }
                currentCommandSteps = Arrays.asList(currentCommand.split("-"));
            }

            System.err.println("currentCommandSteps : " + currentCommandSteps);
            System.err.println("currentStep : " + currentStep);

            //On récupère la prochaine recette a effectuer pour la commande.
            String stepItem = currentStep <= currentCommandSteps.size() - 1 ? currentCommandSteps.get(currentStep) : "CLIENT";
            System.err.println("stepItem :" + stepItem);

            //Si on a pas encore récupéré d'assiette (début de tour)
            if (!playerItem.contains("DISH") && !droppedDish) {
                //Si l'ingredient à réaliser est un ingrédient simple, on prend d'abord une assiette
                if (simpleItems.contains(stepItem)) {
                    getNewDish();
                    makeRecipe(stepItem);
                    currentStep++;
                    //Sinon on effectue d'abord la recette (les recettes complexe ne peuvent pas être réalisées avec une assiette en main)
                } else {
                    makeRecipe(stepItem);
                    currentStep++;
                    getNewDish();
                }

                //Si on a une assiette en main
            } else if (!droppedDish) {
                //Si on a déjà traiter toute la commande, on envois au client.
                if ("CLIENT".equals(stepItem)) {
                    sendToCustomer();
                    currentCommand = "";
                    currentCommandSteps = new ArrayList<>();
                    currentStep = 1;
                    droppedDish = false;
                    //Sinon on traite la prochaine recette
                } else {
                    //Si c'est une recette simple, on fais la recette directe (donc on récupère juste l'ingrédient)
                    if (simpleItems.contains(stepItem)) {
                        makeRecipe(stepItem);
                        currentStep++;
                        //Sinon on pause notre assiete près du four
                        //(car on ne peut avoir que croissant ou tarte. La fraise a été faite avant)
                        //Puis on fait la recette et on pose l'objet sur l'assiette
                        //L'asiette n'est pas récupérée, elle est toujours poser au même endroit
                    } else {
                        droppedDishPosition = dropDish();
                        droppedDish = true;
                        makeRecipe(stepItem);
                        putItemOnDroppedDish(droppedDishPosition);
                        currentStep++;
                    }
                }
                //Si on a une assiette mais qu'elle est posé sur une table(près du four)
            } else {
                //Si on a déjà traiter toute la commande
                //On récupère notre assiette et on envois au client.
                if ("CLIENT".equals(stepItem)) {
                    getDroppedDish(droppedDishPosition);
                    sendToCustomer();
                    currentCommand = "";
                    currentCommandSteps = new ArrayList<>();
                    currentStep = 1;
                    droppedDish = false;
                } else {
                    //Si c'est une recette simple
                    //On récupère notre assiette et on fais la recette (donc on récupère juste l'ingrédient)
                    if (simpleItems.contains(stepItem)) {
                        getDroppedDish(droppedDishPosition);
                        droppedDish = false;
                        makeRecipe(stepItem);
                        currentStep++;
                        //Sinon on fait notre recette directe
                        //Puis on pose l'objet sur l'assiette
                        //L'asiette n'est pas récupérée, elle est toujours poser au même endroit
                    } else {
                        makeRecipe(stepItem);
                        putItemOnDroppedDish(droppedDishPosition);
                        currentStep++;
                    }
                }
            }

            //Ceci est utile pour détecter la fin d'un sprint.
            //On reset tout pour pouvoir reprendre sur de bonne base la partie avec le joueur suivant
            if (turnsRemaining <= 1) {
                System.err.println("TURN IS OVER : RESET ALL");
                resetAll();
            }
        }

    }
    
    public static int getDistanceTo(int[] position, int[] item, boolean partner){
        //Si on veut prendre le partner en considération on met à true
        int minDistance = 1000;
        for (int i = -1; i <= 1; i++) {
            for (int j = -1; j <= 1; j++) {
                String v = (item[0] + i) + "" + (item[1] + j);
                if (floor.contains(v) && ((partner && !v.equals(partnerX+""+partnerY)) || !partner)) {
                    int x = item[0] + i;
                    int y = item[1] + j;
                    int tmp = reachedTiles.get(x+""+y);
                    if(tmp < minDistance) minDistance = tmp;
                }
            }
        }
        return minDistance;
    }
    
    
    public static void getReachableTiles(List<int[]> positions, boolean partner, int counter){
        List<int[]> reachableDirection = new ArrayList<int[]>();
        for(int[] position : positions){
            int x = position[0];
            int y = position[1];
            int[] up = {x,y-1};
            int[] down = {x,y+1};
            int[] left = {x-1,y};
            int[] right = {x+1,y};
            List<int[]> directions = Arrays.asList(up,down,left,right);
            for(int[] direction : directions){
                String dir = direction[0] + "" + direction[1];
                if(!(reachedTiles.keySet().contains(dir)) && floor.contains(dir) && ((partner && !dir.equals(partnerX+""+partnerY)) || !partner)){
                    if((partner && !dir.equals(partnerX+""+partnerY)) || !partner)
                    reachableDirection.add(direction);
                    reachedTiles.put(dir, counter);
                }
            }
        }
        if(reachableDirection.size() > 0)getReachableTiles(reachableDirection, partner, (counter+1));
    }

    //Méthode pour initialiser tous les input d'entrées
    private static void initInput() {
        turnsRemaining = in.nextInt();
        playerX = in.nextInt();
        playerY = in.nextInt();
        playerItem = in.next();
        partnerX = in.nextInt();
        partnerY = in.nextInt();
        partnerItem = in.next();
        numTablesWithItems = in.nextInt(); // the number of tables in the kitchen that currently hold an item
        occupiedTables2 = new HashMap<>();
        for (int i = 0; i < numTablesWithItems; i++) {
            int tableX = in.nextInt();
            int tableY = in.nextInt();
            occupiedTables.add(tableX + "" + tableY);
            String item = in.next();
            occupiedTables2.put(tableX + "" + tableY , item);
        }
        System.err.println("occupiedTables2 :"+occupiedTables2);
        ovenContents = in.next();
        ovenTimer = in.nextInt();
        numCustomers = in.nextInt(); // the number of customers currently waiting for food
        for (int i = 0; i < numCustomers; i++) {
            String customerItem = in.next();
            if (i == 1) secondCommand = customerItem;
            int customerAward = in.nextInt();
        }
        
    }

    public static void resetAll(){
        System.err.println("RESET ALL");
        WAIT();
        currentCommand = "";
        currentCommandSteps = new ArrayList<>();
        currentStep = 1;
        droppedDish = false;
        droppedDishPosition = new int[2];
    }

    //Méthode permettant de déposer un objet sur une assiette lorsque celle-ci est posé sur une table
    private static void putItemOnDroppedDish(int[] droppedDishPosition) {
        if(occupiedTables2.keySet().contains(droppedDishPosition[0] + "" + droppedDishPosition[1])){
            if(occupiedTables2.get(droppedDishPosition[0] + "" + droppedDishPosition[1]).contains(currentCommandSteps.get(currentStep))){
                System.err.println("Item already on dish");
                dropDish();
            } else {
                goToAndUse(droppedDishPosition, "STARTING PUTTING ITEM ON THE DROPPED DISH", "ITEM ON THE DISH");
            }
        } else {
            System.err.println("Ben elle est ou mon assiette ?");
            dropDish();
            resetAll();
        }
    }

    //Méthode permettant de récupérer une assiette posée sur une table (fonctionne uniquement si on a rien en main)
    private static void getDroppedDish(int[] droppedDishPosition) {
        if(occupiedTables2.keySet().contains(droppedDishPosition[0] + "" + droppedDishPosition[1])){
            goToAndUse(droppedDishPosition, "STARTING GETTING BACK THE DROPPED DISH", "DISH BACK IN HAND");
        } else {
            System.err.println("Au voleur ! Il m'a volé mon assiete le batard");
            dropDish();
            resetAll();
        }
    }

    private static void goToAndUse(int[] thing, String beginMessage, String endMessage) {
        goToAndUse(thing, beginMessage, endMessage, false);
    }

    private static void goToAndUse(int[] thing, String beginMessage, String endMessage, boolean inputAtNextRound) {
        System.err.println(beginMessage);
        boolean actionIsDone = false;
        while (!actionIsDone && turnsRemaining > 1) {
            if (isCloseTo(thing)) {
                use(thing);
                actionIsDone = true;
            } else {
                moveTo(thing);
                if (inputAtNextRound) initInput();
            }
            if (!inputAtNextRound) initInput();
        }
        System.err.println(endMessage);
    }

    //Méthode permettant de finaliser une commande
    private static void sendToCustomer() {
        goToAndUse(client, "STARTING SENDING TO CUSTOMER", "CommandSent", true);
    }


    private static void WAIT() {
        System.out.println("WAIT");
    }

    private static void use(int[] item) {
        use(item[0], item[1]);
    }

    private static void use(int x, int y) {
        System.out.println("USE " + x + " " + y);
    }

    private static void moveTo(int[] item) {
        moveTo(item[0], item[1]);
    }

    private static void moveTo(int x, int y) {
        System.out.println("MOVE " + x + " " + y);
    }

    //Méthode permettant de déposer son assiette près du four
    //Amélioration : Trouver le meilleur endroit pour déposer son assiette
    //Car près du four est rarement le bon endroit en entraine souvent des trajets inutile
    private static int[] dropDish() {
        System.err.println("STARTING DROPPING THE DISH NEAR THE OVEN");
        int[] droppedDishPosition = new int[2];
        boolean dishDropped = false;
        while (!dishDropped && turnsRemaining > 1) {
            if (isCloseTo(oven)) {
                //Vérifier les cases 8 cases autour de joueurs pour vérifier si elles peuvent accueillir l'assiette

                // FIXME : Probleme lorsque qu'il n'y a aucune place pour poser l'assiette.
                int x = -1;
                int y = -1;
                for (int i = -1; i <= 1; i++) {
                    for (int j = -1; j <= 1; j++) {
                        String v = (playerX + i) + "" + (playerY + j);
                        if (freeTables.contains(v) && !occupiedTables.contains(v)) {
                            x = playerX + i;
                            y = playerY + j;
                            break;
                        }
                    }
                    if (x != -1 && y != -1) break;
                }

                if (x == -1 && y == -1) {
                    if (oven[0] == 0 || oven[0] == 10) {
                        if (playerY <= oven[1]) {
                            moveTo(playerX, (oven[1] + 1));
                        } else {
                            moveTo(playerX, (oven[1] - 1));
                        }
                    } else {
                        if (playerX <= oven[0]) {
                            moveTo((oven[0] + 1), playerY);
                        } else {
                            moveTo((oven[0] - 1), playerY);
                        }
                    }
                } else {
                    use(x, y);
                    droppedDishPosition[0] = x;
                    droppedDishPosition[1] = y;
                    dishDropped = true;
                }
            } else {
                moveTo(oven);
            }
            initInput();
        }
        System.err.println("DISH DROPPED NEAR THE OVEN");
        return droppedDishPosition;
    }

    //Méthode permettant de récupérer une nouvelle assiette
    private static void getNewDish() {
        goToAndUse(dishes, "STARTING GETTING A NEW DISH", "NEW DISH IN HAND");
    }

    private static boolean isCloseTo(int[] item) {
        return Math.abs(playerX - item[0]) <= 1 && Math.abs(playerY - item[1]) <= 1;
    }

    //Méthode permettant de traiter une recette (à partir du cahier de recette)
    //Amélioration : Gestion du four et du temps d'attente
    //Il serait bon d'effectuer des actions lorsque le four est occupé plutôt que d'attendre devant qu'il cuise (perte de temps)
    //Attention à ne pas laisser cramer l'objet de la recette.
    private static void makeRecipe(String recipe) {
        System.err.println("STARTING RECIPE : " + recipe);
        List<int[]> itemList = recipes.get(recipe);
        for (int[] item : itemList) {
            boolean recipeDone = false;
            while (!recipeDone && turnsRemaining > 1) {
                //System.err.println("turnsRemaining makeRecipe :" + turnsRemaining);
                if (item == oven) {
                    System.err.println("I should WAIT for the oven to finish the cooking");
                    if (isCloseTo(item)) {
                        if ("NONE".equals(playerItem) && ("DOUGH".equals(ovenContents) || "RAW_TART".equals(ovenContents))
                                || (!"NONE".equals(playerItem) && !"NONE".equals(ovenContents))) {
                            WAIT();
                        } else if ("CROISSANT".equals(ovenContents) || "TART".equals(ovenContents)) {
                            use(item);
                            recipeDone = true;
                        } else {
                            use(item);
                        }
                    } else {
                        moveTo(item);
                    }
                } else {
                    if (isCloseTo(item)) {
                        use(item);
                        recipeDone = true;
                    } else {
                        moveTo(item);
                    }
                }
                initInput();
            }
        }
        System.err.println("RECIPE DONE : " + recipe);
    }
}